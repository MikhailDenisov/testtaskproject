using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FieldView : MainView
{
    [SerializeField] private Button generateFieldButton;
    [SerializeField] private List<GameObject> _grids;
    [SerializeField] private Material _selectedMaterial, _defaultMaterial, _wrongValueMaterial, _selectedBrightMaterial;
    [SerializeField] private TextMeshProUGUI ResultText;
    [SerializeField] private TMP_Dropdown difficultyDropdown;
    private GameObject _targetTile;
    private readonly List<TileId> _tilesList = new();
    private List<TileId> _relatedTilesList = new();
    private List<TileId> _wrongTilesList = new();


    private void Start()
    {
        generateFieldButton.onClick.AddListener(GenerateGameField);
        StartGame();
    }

    protected override void StartGame()
    {
        GenerateGameField();
    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            HandleClick();
        }
    }

    private void GenerateGameField()
    {
        ResultText.gameObject.SetActive(false);
        _presenter.GenerateGameFieldValues();
        FillAllTiles();
    }


    private void FillAllTiles()
    {
        int i = 0, j = 0, m = 1;
        foreach (var grid in _grids)
        {
            if (grid.GetComponent<GridTiles>() != null)
            {
                FillGrid(grid.GetComponent<GridTiles>());
            }
        }
    }

    public void SetSudokuDifficulty()
    {
        _presenter.ChangeDifficulty(difficultyDropdown.value);
    }
    

    private void FillGrid(GridTiles grid)
    {
        int c = 0, yOffset = grid.GridYOffset, xOffset = grid.GridXOffset;
        for (var i = 0 + 3 * yOffset; i < 3 + 3 * yOffset; i++)
        {
            for (var j = 0 + 3 * xOffset; j < 3 + 3 * xOffset; j++)
            {
                var val = _presenter.GetSudokuValue(i, j);
                var tile = grid.GridTilesList[c].GetComponent<TileId>();
                tile.Raw = i;
                tile.Column = j;
                tile.GridId = grid.GridId;
                tile.TileValue = val;
                tile.SetColorToDefault();
                if (val == 0)
                {
                    tile.IsDefaultValue = false;
                    tile.valueText.text = string.Empty;
                }
                else
                {
                    tile.IsDefaultValue = !_presenter.IsValueChanged(tile.Raw, tile.Column);
                    tile.valueText.text = val.ToString();
                }

                _tilesList.Add(tile);
                c++;
            }
        }
    }

    private List<TileId> FindRelatedTiles(TileId tile)
    {
        var selectedTile = tile;
        _relatedTilesList = _tilesList.Where(x =>
            (x.Raw == selectedTile.Raw || x.Column == selectedTile.Column || x.GridId == selectedTile.GridId) &&
            x != selectedTile).ToList();
        return _relatedTilesList;
    }

    private void SetMaterialRelatedTiles(Material material)
    {
        var tiles = FindRelatedTiles(_targetTile.GetComponent<TileId>());
        foreach (var tile in tiles)
        {
            tile.transform.GetComponent<MeshRenderer>().material = material;
        }
    }

    public void CheckIsSudokuSolved()
    {
        ResultText.gameObject.SetActive(true);
        SetResultText(_presenter.IsSudokuComplete());
    }

    private void SetResultText(bool result)
    {
        switch (result)
        {
            case true:
                ResultText.text = "Вы победили";
                ResultText.color = Color.green;
                break;
            case false:
                ResultText.text = "Судоку заполнено неправильно";
                ResultText.color = Color.red;
                break;
        }
    }

    private void CheckIsValueCorrect(TileId tile)
    {
        var result = _relatedTilesList.Where(x => x.TileValue == tile.TileValue).ToList();
        if (result.Any())
        {
            tile.valueText.color = tile.IsDefaultValue ? Color.red : Color.magenta;
        }
        else
        {
            tile.valueText.color = tile.IsDefaultValue ? tile.DefaultColor : Color.green;
        }
    }

    public void LoadSudokuView()
    {
        _presenter.LoadData();
        FillAllTiles();
        CheckField();
    }

    public void SaveSudokuView()
    {
        _presenter.SaveData();
    }

    public void ChangeTileValue(int val)
    {
        if (_targetTile == null) return;
        var tile = _targetTile.GetComponent<TileId>();
        tile.SetValue(val);
        _presenter.ChangeSudokuValue(tile.Raw, tile.Column, tile.TileValue);
        CheckField();
    }

    private void CheckField()
    {
        foreach (var checkTile in _tilesList)
        {
            FindRelatedTiles(checkTile);
            CheckIsValueCorrect(checkTile);
        }
    }

    private void HandleClick()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            var slot = hit.collider.GetComponent<TileId>();
            if (slot != null)
            {
                if (_targetTile != slot.transform.gameObject)
                {
                    if (_targetTile != null)
                    {
                        _targetTile.transform.GetComponent<MeshRenderer>().material = _defaultMaterial;
                        SetMaterialRelatedTiles(_defaultMaterial);
                    }

                    if (slot.IsDefaultValue)
                    {
                        _targetTile = null;
                        return;
                    }

                    _targetTile = slot.transform.gameObject;
                    SetMaterialRelatedTiles(_selectedMaterial);
                    _targetTile.transform.GetComponent<MeshRenderer>().material = _selectedBrightMaterial;
                }
                //_presenter.OnTileClicked(slot.SlotID);
            }
        }
        else
        {
            if (_targetTile != null)
            {
                SetMaterialRelatedTiles(_defaultMaterial);
                _targetTile.transform.GetComponent<MeshRenderer>().material = _defaultMaterial;
                _targetTile = null;
            }
        }
    }
}