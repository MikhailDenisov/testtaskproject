using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTiles : MonoBehaviour
{
   [SerializeField] private List<GameObject> _gridTiles;
   public List<GameObject> GridTilesList => _gridTiles;
   [SerializeField] private int _gridId;
   public int GridId => _gridId;
   [SerializeField] private int _gridXOffset;
   public int GridXOffset => _gridXOffset;
   [SerializeField] private int _gridYOffset;
   public int GridYOffset => _gridYOffset;
}
