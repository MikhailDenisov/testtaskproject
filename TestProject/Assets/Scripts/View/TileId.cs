using System;
using TMPro;
using UnityEngine;

public class TileId : MonoBehaviour
{
    [SerializeField] private int _tileId;
    public int SlotID => _tileId;
    [NonSerialized] public int TileValue;
    [NonSerialized] public bool IsDefaultValue;
    [SerializeField] public Color DefaultColor = Color.blue;
    [SerializeField] public TextMeshPro valueText;

    [SerializeField]
    public int Raw, Column, GridId;

    public void SetValue(int val)
    {
        TileValue = val;
        valueText.text = val.ToString();
        valueText.color = Color.green;
    }

    public void SetColorToDefault()
    {
        valueText.color = DefaultColor;
    }
}