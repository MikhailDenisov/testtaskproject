using UnityEngine;

public abstract class MainView : MonoBehaviour
{
  protected MainPresenter _presenter;

  public void Init(MainPresenter presenter)
  {
    _presenter = presenter;
  }

  protected abstract void StartGame();
}
