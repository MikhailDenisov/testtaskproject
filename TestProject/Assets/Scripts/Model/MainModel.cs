using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class MainModel
{
    protected MainView _view;
    private GameData _gameData;
    // private int[,] _sudoku = new int[9, 9];
    // private int[,] _changedSudoku = new int[9, 9];
    //
    // //private int[] test = new int[5];
    // private int[,] _solvedSudoku = new int[9, 9];
    protected MainModel(MainView view)
    {
        _view = view;
    }
    public void ChangeSudokuValue(int raw, int column, int value)
    {
        _gameData.Sudoku[raw, column] = value;
        _gameData.ChangedSudoku[raw, column] = value;
    }
    public bool IsSudokuFilled()
    {
        var isSudokuComplete = !_gameData.Sudoku.Cast<int>().Contains(0);
        return isSudokuComplete;
    }

    public bool IsSudokuSolvedCorrectly()
    {
        var result = _gameData.Sudoku.Cast<int>().SequenceEqual(_gameData.SolvedSudoku.Cast<int>());
        return result;
    }

    public int GetSudokuValue(int i, int j)
    {
        return _gameData.Sudoku[i, j];
    }

    public void SetSudoku(int[,] sudoku, int[,] solvedSudoku, int[,] changedSudoku)
    {
        _gameData = new GameData()
        {
            Sudoku = sudoku,
            SolvedSudoku = solvedSudoku,
            ChangedSudoku = changedSudoku
        };
        // _sudoku = sudoku;
        // _solvedSudoku = solvedSudoku;
        // _changedSudoku = changedSudoku;
    }

    public GameData GetGameData()
    {
        return _gameData;
    }

    public bool IsValueChanged(int i, int j)
    {
        return _gameData.ChangedSudoku[i, j] != 0;
    }
}

public class GameData
{
    public int[,] SolvedSudoku { get; set; }
    public int[,] Sudoku { get; set; }
    public int[,] ChangedSudoku { get; set; }
}