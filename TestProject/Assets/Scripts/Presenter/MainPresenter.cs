using System;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public abstract class MainPresenter
{
    // [SerializeField] private string _saveDataName;
    private readonly MainModel _model;
    private SudokuDifficulty _difficulty = SudokuDifficulty.Easy;

    protected MainPresenter(MainModel model)
    {
        _model = model;
    }

    public void GenerateGameFieldValues()
    {
        var generator = new GeneratorService();
        var emptyTiles = _difficulty switch
        {
            SudokuDifficulty.Easy => 10,
            SudokuDifficulty.Medium => 25,
            SudokuDifficulty.Hard => 40,
            _ => 0
        };

        _model.SetSudoku(generator.GetSudoku(emptyTiles), generator.GetSolvedSudoku(), new int[9, 9]);
    }

    public bool IsSudokuComplete()
    {
        if (_model.IsSudokuFilled())
        {
            if (_model.IsSudokuSolvedCorrectly())
                return true;
        }

        return false;
    }

    public void ChangeSudokuValue(int raw, int column, int value)
    {
        _model.ChangeSudokuValue(raw, column, value);
    }

    public int GetSudokuValue(int i, int j)
    {
        return _model.GetSudokuValue(i, j);
    }

    public bool IsValueChanged(int i, int j)
    {
        return _model.IsValueChanged(i, j);
    }

    public void LoadData()
    {
        var path = Path.Combine(Application.persistentDataPath, "saveData.data");
        GameData loadedData = null;
        if (!File.Exists(path)) return;
        try
        {
            using (var stream = new FileStream(path, FileMode.Open))
            {
                string dataToLoad;
                using (var reader = new StreamReader(stream))
                {
                    dataToLoad = reader.ReadToEnd();
                }

                loadedData = JsonConvert.DeserializeObject<GameData>(dataToLoad,
                    new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Auto });
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Error occured when trying to load data from file:" + path + "\n" + e);
        }

        if (loadedData != null)
            _model.SetSudoku(loadedData.Sudoku, loadedData.SolvedSudoku, loadedData.ChangedSudoku);
    }

    public void SaveData()
    {
        var data = _model.GetGameData();
        var path = Path.Combine(Application.persistentDataPath, "saveData.data");
        try
        {
            var dataToStore = JsonConvert.SerializeObject(data,
                new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Auto });

            using (var stream = new FileStream(path, FileMode.Create))
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(dataToStore);
                }
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Error occured when trying to save data to file:" + path + "\n" + e);
        }
    }

    public void ChangeDifficulty(int value)
    {
        switch (value)
        {
            case 0:
                _difficulty = SudokuDifficulty.Easy;
                break;
            case 1:
                _difficulty = SudokuDifficulty.Medium;
                break;
            case 2:
                _difficulty = SudokuDifficulty.Hard;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private enum SudokuDifficulty
    {
        Easy = 20,
        Medium = 30,
        Hard = 40
    }
}