using UnityEngine;

public class Startupper : MonoBehaviour
{
    [SerializeField] private MainView _view;
    private void Awake()
    {
        var view = _view;
        var model = new DefaultMainModel(_view);
        var presenter = new DefaultMainPresenter(model);
        view.Init(presenter);
    }
}
