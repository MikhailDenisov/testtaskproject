using System;

public class GeneratorService
{
    private readonly Random random = new Random();
    private readonly int[,] sudoku = new int[9, 9];
    private int[,] solvedSudoku = new int[9, 9];
    private int _emptyTiles = 0;

    public int[,] GetSudoku(int emptyTiles)
    {
        _emptyTiles = emptyTiles;
        FillSudoku();
        return sudoku;
    }

    public int[,] GetSolvedSudoku()
    {
        return solvedSudoku;
    }

    // public int[,] GenerateSudoku()
    // {
    //    // sudoku = new int[9, 9];
    //     FillSudoku();
    //     return sudoku;
    // }

    private void FillSudoku()
    {
        FillDiagonals();
        SolveSudoku();
        Array.Copy(sudoku,solvedSudoku,sudoku.Length);
        RemoveCells();
    }

    private void FillDiagonals()
    {
        for (int i = 0; i < 9; i += 3)
        {
            FillBox(i, i);
        }
    }

    private void FillBox(int startRow, int startCol)
    {
        int num;
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                do
                {
                    num = random.Next(1, 10);
                } while (!IsSafe(startRow + row, startCol + col, num));

                sudoku[startRow + row, startCol + col] = num;
            }
        }
    }

    private bool IsSafe(int row, int col, int num)
    {
        return !UsedInRow(row, num) &&
               !UsedInCol(col, num) &&
               !UsedInBox(row - row % 3, col - col % 3, num);
    }

    private bool UsedInRow(int row, int num)
    {
        for (int col = 0; col < 9; col++)
        {
            if (sudoku[row, col] == num)
            {
                return true;
            }
        }
        return false;
    }

    private bool UsedInCol(int col, int num)
    {
        for (int row = 0; row < 9; row++)
        {
            if (sudoku[row, col] == num)
            {
                return true;
            }
        }
        return false;
    }

    private bool UsedInBox(int boxStartRow, int boxStartCol, int num)
    {
        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                if (sudoku[row + boxStartRow, col + boxStartCol] == num)
                {
                    return true;
                }
            }
        }
        return false;
    }

    private bool SolveSudoku()
    {
        for (int row = 0; row < 9; row++)
        {
            for (int col = 0; col < 9; col++)
            {
                if (sudoku[row, col] == 0)
                {
                    for (int num = 1; num <= 9; num++)
                    {
                        if (IsSafe(row, col, num))
                        {
                            sudoku[row, col] = num;
                            if (SolveSudoku())
                            {
                                return true;
                            }
                            sudoku[row, col] = 0;
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }
    
    private void RemoveCells()
    {
        int count = _emptyTiles; 

        while (count > 0)
        {
            int row = random.Next(0, 9);
            int col = random.Next(0, 9);

            if (sudoku[row, col] != 0)
            {
                sudoku[row, col] = 0;
                count--;
            }
        }
    }
}
