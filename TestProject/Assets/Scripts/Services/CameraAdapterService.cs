using System;
using UnityEngine;

public class CameraAdapterService : MonoBehaviour
{
  // [SerializeField] private GameObject mainCamera;
   private void Awake()
   {
      var screenCoeff = Screen.height / (float)Screen.width;
      if (screenCoeff > 2)
      {
         screenCoeff -= 1;
         var position = gameObject.transform.position;
         position = new Vector3(position.x, position.y, position.z*screenCoeff);
         gameObject.transform.position = position;
      }
   }
}
